package javamvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import mvc.View;

public class BaseFrame extends JFrame implements View {
    private static final long serialVersionUID = 3308598603596839574L;

    private JTextField searchTF;
    private JTextArea outputTA;
    private LookupController controller;

    public BaseFrame(LookupController controller) {
        this.controller = controller;

        initComponents();
        layoutComponents();

        controller.addView(this);
    }

    private void initComponents() {
        searchTF = new JTextField();
        outputTA = new JTextArea();
        outputTA.setEditable(false);
    }

    private void layoutComponents() {
        JLabel searchLabel = new JLabel("Search");
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton searchButton = new JButton("Go");

        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                controller.searchButton_actionPerformed(searchTF.getText());
            }
        });

        getContentPane().setLayout(new BorderLayout());

        searchLabel.setPreferredSize(new Dimension(45, 26));
        topPanel.add(searchLabel);
        searchTF.setPreferredSize(new Dimension(100, 26));
        topPanel.add(searchTF);
        topPanel.add(searchButton);

        getContentPane().add(topPanel, BorderLayout.NORTH);
        getContentPane().add(outputTA, BorderLayout.CENTER);

        setLocation(200, 200);
        setSize(300, 300);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    @Override
    public void modelPropertyChange(PropertyChangeEvent evt) {
        final Object newValue = evt.getNewValue();

        if (LookupController.SEARCH_RESULT_PROPERTY.equals(evt
                .getPropertyName())) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    outputTA.setText((String) newValue);
                }
            });
        }
    }
}
