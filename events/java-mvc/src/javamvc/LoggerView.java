package javamvc;

import java.beans.PropertyChangeEvent;

import mvc.View;

public class LoggerView implements View {

    @Override
    public void modelPropertyChange(PropertyChangeEvent evt) {
        final Object newValue = evt.getNewValue();

        if (LookupController.SEARCH_RESULT_PROPERTY.equals(evt
                .getPropertyName())) {
            System.out.println("Search Results fired, " + (String) newValue);
        }
    }

}
