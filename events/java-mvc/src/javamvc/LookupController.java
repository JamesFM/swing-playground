package javamvc;

import java.util.Random;

import mvc.AbstractController;

public class LookupController extends AbstractController {
    public static final String SEARCH_RESULT_PROPERTY = "SearchResults";

    public void lookup(String text) {
        // mimic the server call delay...
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // imagine we got this from a server
        Random rand = new Random();
        String[] results = new String[] { "Book " + rand.nextInt(50), 
                "Book " + rand.nextInt(50), "Book " + rand.nextInt(50) };

        // there are better ways to do this
        String resultStr = "";
        for (String r : results) {
            resultStr += "\n" + r;
        }
        setModelProperty(SEARCH_RESULT_PROPERTY, resultStr);
    }

    public void searchButton_actionPerformed(final String text) {
        final LookupController controller = this;
        new Thread() {
            public void run() {
                controller.lookup(text);
            }
        }.start();
    }

}
