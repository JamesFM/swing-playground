package javamvc;

public class Main {

    public static void main(String[] args) {
        LookupController c = new LookupController();
        BaseFrame baseView = new BaseFrame(c);
        baseView.setVisible(true);
        c.addModel(new LookupModel());
        c.addView(baseView);
        c.addView(new LoggerView());
    }

}
