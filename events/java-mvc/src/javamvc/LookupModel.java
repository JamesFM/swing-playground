package javamvc;

import mvc.AbstractModel;

public class LookupModel extends AbstractModel {

    private String searchResults;

    public String getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(String searchResults) {
        String oldValue = this.searchResults;
        this.searchResults = searchResults;

        firePropertyChange(LookupController.SEARCH_RESULT_PROPERTY, oldValue,
                searchResults);
    }

}
