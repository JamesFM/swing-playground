package scalamvc

import java.beans.PropertyChangeEvent
import mvc.View
import javax.swing.SwingUtilities
import java.awt.event.ActionListener
import java.awt.event.ActionEvent

class BaseView(controller: LookupController) extends BaseFrame with View {

  controller.addView(this)

  // FIXME do this in a more scala friendly way
  searchButton.addActionListener(new ActionListener {
    override def actionPerformed(e: ActionEvent) {
      controller.searchButton_actionPerformed(searchTF.getText())
    }
  })

  override def modelPropertyChange(evt: PropertyChangeEvent) = {
    // from http://stackoverflow.com/questions/931463
    /*
    evt.getNewValue() match {
      case newValue: String => newValue
      case _ => throw new ClassCastException
    }
    */
    val newValue = evt.getNewValue().asInstanceOf[String]

    if (LookupController.SEARCH_RESULT_PROPERTY.equals(evt.getPropertyName())) {
      SwingUtilities.invokeLater(new Runnable {
        override def run() = {
          outputTA.setText(newValue)
        }
      })
    }
  }

}