package scalamvc

import mvc.View
import java.beans.PropertyChangeEvent

class LoggerView extends View {

  override def modelPropertyChange(evt: PropertyChangeEvent) {
    if (LookupController.SEARCH_RESULT_PROPERTY.equals(evt.getPropertyName())) {
      println("Search Results fired, " + evt.getNewValue())
    }
  }
}