package scalamvc

object Main {

  def main(args: Array[String]) = {
    val c = new LookupController
    val baseView = new BaseView(c)
    baseView.setVisible(true)
    c.addModel(new LookupModel)
    c.addView(baseView)
    c.addView(new LoggerView)
  }

}