package scalamvc

import mvc.Controller
import scala.util.Random

object LookupController {
  val SEARCH_RESULT_PROPERTY: String = "searchResults"
}

class LookupController extends Controller {

  def lookup(text: String) = {
    // mimic the server call delay...
    try {
      Thread.sleep(2000)
    } catch {
      case e: Exception => e.printStackTrace()
    }
    val r:Random = new Random
    val results = Array("Book " + r.nextInt(50), 
        "Book " + r.nextInt(50), "Book " + r.nextInt(50))
    val resultStr = results.foldLeft("")((total, n) => total + " " + n)

    setModelProperty(LookupController.SEARCH_RESULT_PROPERTY, resultStr)
  }

  def searchButton_actionPerformed(text: String) {
    val controller = this
    new Thread {
      override def run = controller.lookup(text)
    }.start
  }
}