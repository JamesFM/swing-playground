package scalamvc

import mvc.Model

class LookupModel extends Model {

  private var _searchText: String = _
  private var _searchResults: String = _

  def searchResults = _searchResults
  def searchResults_=(value: String) = {
    val oldValue = _searchResults
    _searchResults = value
    firePropertyChange(LookupController.SEARCH_RESULT_PROPERTY, oldValue, value)
  }
}