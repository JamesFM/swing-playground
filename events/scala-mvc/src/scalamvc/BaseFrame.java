package scalamvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BaseFrame extends JFrame {
    private static final long serialVersionUID = 3308598603596839574L;

    protected JTextField searchTF;
    protected JTextArea outputTA;
    protected JButton searchButton;

    public BaseFrame() {
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        searchTF = new JTextField();
        outputTA = new JTextArea();
        outputTA.setEditable(false);
    }

    private void layoutComponents() {
        JLabel searchLabel = new JLabel("Search");
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        searchButton = new JButton("Go");

        getContentPane().setLayout(new BorderLayout());

        searchLabel.setPreferredSize(new Dimension(45, 26));
        topPanel.add(searchLabel);
        searchTF.setPreferredSize(new Dimension(100, 26));
        topPanel.add(searchTF);
        topPanel.add(searchButton);

        getContentPane().add(topPanel, BorderLayout.NORTH);
        getContentPane().add(outputTA, BorderLayout.CENTER);

        setLocation(200, 200);
        setSize(300, 300);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
