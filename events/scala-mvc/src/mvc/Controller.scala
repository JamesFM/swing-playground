package mvc

import java.beans.PropertyChangeEvent
import java.beans.PropertyChangeListener

import scala.reflect.Method

trait Controller extends PropertyChangeListener {
  // TODO review usage of Scala collections here
  private var views = Set[View]()
  private var models = Set[Model]()

  override def propertyChange(evt: PropertyChangeEvent) =
    views.foreach(view => view.modelPropertyChange(evt))

  def addModel(model: Model) = {
    models += model
    model.addPropertyChangeListener(this)
  }

  def removeModel(model: Model) = {
    models -= model
    model.removePropertyChangeListener(this)
  }

  def addView(view: View) = views += view

  def removeView(view: View) = views -= view

  // Scala is magical
  // taken from http://stackoverflow.com/a/1589919/20774
  implicit def reflector(ref: AnyRef) = new {
    def getV(name: String): Any = ref.getClass.getMethods.find(_.getName == name).get.invoke(ref)
    def setV(name: String, value: Any): Unit = ref.getClass.getMethods.find(_.getName == name + "_$eq").get.invoke(ref, value.asInstanceOf[AnyRef])
  }

  protected def setModelProperty(property: String, newValue: AnyRef) = {
    models.foreach { model =>
      try {
        // model.getClass.getMethods.find(_.getName == property + "_$eq").get.invoke(model, newValue.asInstanceOf[AnyRef])
        model.setV(property, newValue)
      } catch {
        case e: Exception => e.printStackTrace()
      }
    }
  }
}