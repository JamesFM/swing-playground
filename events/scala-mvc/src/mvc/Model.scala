package mvc

import java.beans.PropertyChangeSupport
import java.beans.PropertyChangeListener

trait Model {

  protected val propertyChangeSupport = new PropertyChangeSupport(this)

  def addPropertyChangeListener(l: PropertyChangeListener) =
    propertyChangeSupport.addPropertyChangeListener(l)

  def removePropertyChangeListener(l: PropertyChangeListener) =
    propertyChangeSupport.removePropertyChangeListener(l)

  // double check use of any here, might be preferable to use AnyRef instead
  protected def firePropertyChange(propertyName: String, oldValue: Any, newValue: Any) =
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
}