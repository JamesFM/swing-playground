package mvc

import java.beans.PropertyChangeEvent

trait View {

  def modelPropertyChange(evt: PropertyChangeEvent)
}