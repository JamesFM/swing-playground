/**
 * Created by IntelliJ IDEA.
 * User: Jonathan Simon
 * Date: Oct 21, 2003
 * Time: 7:47:14 AM
 * To change this template use Options | File Templates.
 */
package webexample;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;

public class LookupManager {
    private Collection listeners = new ArrayList();

    public void lookup(String text) {
        fireLookupStarted(text);
        //mimic the server call delay...
        try {
            Thread.sleep(5000);
        } catch (Exception e){
            e.printStackTrace();
        }
        //imagine we got this from a server
        String[] results = new String[]{"Book one", "Book two", "Book three"};
        fireLookupCompleted(text, results);

    }

    public void addListener(LookupListener listener){
        listeners.add(listener);
    }

    public void removeListener(LookupListener listener){
        listeners.remove(listener);
    }

    private void fireLookupStarted(String searchText){
        LookupEvent event = new LookupEvent(searchText);
        Iterator iter = new ArrayList(listeners).iterator();
        while (iter.hasNext()) {
            LookupListener listener = (LookupListener) iter.next();
            listener.lookupStarted(event);
        }
    }

    private void fireLookupCompleted(String searchText, String[] results){
        LookupEvent event = new LookupEvent(searchText, results);
        Iterator iter = new ArrayList(listeners).iterator();
        while (iter.hasNext()) {
            LookupListener listener = (LookupListener) iter.next();
            listener.lookupCompleted(event);
        }
    }

}
