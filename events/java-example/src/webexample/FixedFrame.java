/**
 * Created by IntelliJ IDEA.
 * User: Jonathan Simon
 * Date: Oct 21, 2003
 * Time: 7:46:59 AM
 * To change this template use Options | File Templates.
 */
package webexample;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class FixedFrame extends JFrame implements LookupListener {
	private static final long serialVersionUID = 3308598603596839574L;
	
	JTextField searchTF;
    JTextArea outputTA;
    LookupManager lookupManager;

    public FixedFrame() {
        lookupManager = new LookupManager();
        lookupManager.addListener(this);
        lookupManager.addListener(new LoggerListener());
        initComponents();
        layoutComponents();
    }

    private void initComponents() {
        searchTF = new JTextField();
        outputTA = new JTextArea();
        outputTA.setEditable(false);
    }

    private void layoutComponents() {
        JLabel searchLabel = new JLabel("Search");
        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton searchButton = new JButton("Go");

        searchButton.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        searchButton_actionPerformed();
                    }
                }
        );

        getContentPane().setLayout(new BorderLayout());

        searchLabel.setPreferredSize(new Dimension(45, 26));
        topPanel.add(searchLabel);
        searchTF.setPreferredSize(new Dimension(100, 26));
        topPanel.add(searchTF);
        topPanel.add(searchButton);

        getContentPane().add(topPanel, BorderLayout.NORTH);
        getContentPane().add(outputTA, BorderLayout.CENTER);

        setLocation(200,200);
        setSize(300,300);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void searchButton_actionPerformed() {
        new Thread(){
            public void run() {
                lookupManager.lookup(searchTF.getText());
            }
        }.start();
    }

    public void lookupStarted(final LookupEvent e) {
        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        outputTA.setText("Searching for: " + e.getSearchText());
                    }
                }
        );
    }

    public void lookupCompleted(final LookupEvent e) {
        SwingUtilities.invokeLater(
                new Runnable() {
                    public void run() {
                        outputTA.setText("");
                        String[] results = e.getResults();
                        for (int i = 0; i < results.length; i++) {
                            String result = results[i];
                            outputTA.setText(outputTA.getText() + "\n" + result);
                        }
                    }
                }
        );
    }

    public static void main(String[] args) {
        new FixedFrame().setVisible(true);
    }

}
