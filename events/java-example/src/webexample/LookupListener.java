/**
 * Created by IntelliJ IDEA.
 * User: Jonathan Simon
 * Date: Oct 21, 2003
 * Time: 7:52:20 AM
 * To change this template use Options | File Templates.
 */
package webexample;

public interface LookupListener {

    public void lookupStarted(LookupEvent e);
    public void lookupCompleted(LookupEvent e);

}
