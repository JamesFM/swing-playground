/**
 * Created by IntelliJ IDEA.
 * User: Jonathan Simon
 * Date: Oct 21, 2003
 * Time: 7:51:56 AM
 * To change this template use Options | File Templates.
 */
package webexample;

public class LookupEvent {
    String searchText;
    String[] results;

    public LookupEvent(String searchText) {
        this.searchText = searchText;
    }

    public LookupEvent(String searchText, String[] results) {
        this.searchText = searchText;
        this.results = results;
    }

    public String getSearchText() {
        return searchText;
    }

    public String[] getResults() {
        return results;
    }

}
