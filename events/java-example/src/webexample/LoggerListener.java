/**
 * Created by IntelliJ IDEA.
 * User: Jonathan Simon
 * Date: Oct 21, 2003
 * Time: 11:09:12 PM
 * To change this template use Options | File Templates.
 */
package webexample;

public class LoggerListener implements LookupListener {

    public void lookupStarted(LookupEvent e) {
        System.out.println("Lookup started: " + e.getSearchText());
    }

    public void lookupCompleted(LookupEvent e) {
        System.out.println("Lookup completed: " + e.getSearchText() + " " + e.getResults());
    }

}
